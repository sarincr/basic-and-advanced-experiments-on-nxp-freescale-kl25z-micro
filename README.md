The FRDM-KL25Z is an ultra-low-cost development platform for Kinetis L Series KL1x (KL14/15) and KL2x (KL24/25) MCUs built on ARM® Cortex™-M0+ processor. Features include easy access to MCU I/O, battery-ready, low-power operation, a standard-based form factor with expansion board options and a built-in debug interface for flash programming and run-control. The FRDM-KL25Z is supported by a range of NXP and third-party development software.
The FRDM-KL25Z has been designed by NXP in collaboration with mbed for prototyping all sorts of devices, especially those requiring the size and price point offered by Cortex-M0+ and the power of USB Host and Device. It is packaged as a development board with connectors to break out to strip board and breadboard, and includes a built-in USB FLASH programmer.
Overview

The Freedom KL25Z is an ultra-low-cost development platform for Kinetis® L Series KL1x (KL14/15) and KL2x (KL24/25) MCUs built on Arm® Cortex®-M0+ processor.

    Features include easy access to MCU I/O, battery-ready, low-power operation, a standards-based form factor with expansion board options and a built-in debug interface for flash programming and run-control.
    The FRDM-KL25Z is supported by a range of NXP® and third-party development software.
    You can now use mbed.org at no charge, with full access to the online SDK, tools, reusable code—which means no downloads, installations or licenses—and an active community of developers.
    Processor Expert® component for low voltage H-Bridge products enables rapid embedded application development.
    The FRDM-KL25Z is supported by Zephyr OS for developing the Internet of Things with a free, open-source embedded operating system. Click here to learn more.

